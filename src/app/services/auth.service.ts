import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private loginUrl = "http://localhost:8080/login"
  private registerUrl = "http://localhost:8080/register"

  constructor(private http:HttpClient,private _router: Router) { }
  loginUser(user){
    console.log('login')
    return this.http.post<any>(this.loginUrl, user);
  }

  isLoggedIn() {
    return (!!localStorage.getItem('token'))
  }

  logoutUser() {
    localStorage.removeItem('token')
    this._router.navigate(['/login'])
  }

  getToken() {
    return localStorage.getItem('token')
  }

  registerUser(user){
    return this.http.post<any>(this.registerUrl, user)
  }

}
