import { Injectable } from "@angular/core";
import { User } from "../models/model";
import { Observable, of } from "rxjs";
import { HttpClient } from "@angular/common/http";
import { tap, map, catchError } from "rxjs/operators";

const CREATE_ACTION = "create";
const UPDATE_ACTION = "update";
const REMOVE_ACTION = "destroy";

@Injectable({
  providedIn: "root",
})
export class Edit {
  users: Observable<User[]>;

  constructor(private http: HttpClient) {}

  getUsers(): Observable<User[]> {
    return this.http
      .get<User[]>(`http://localhost:8080/fetchUsers`, { responseType: "json" })
      .pipe(tap((_) => console.log("error on request")));
  }

  public save(data: any, isNew?: boolean) {
    const action = isNew ? CREATE_ACTION : UPDATE_ACTION;
    this.reset();
    return this.http
      .post<User[]>(`http://localhost:8080/${action}`, data,{ responseType: "json" })
      .pipe(tap((_) => console.log("error on request")));
  }

  public remove(data: any) {
    this.reset();
    return this.http
    .post<User[]>(`http://localhost:8080/remove`, data,{ responseType: "json" })
    .pipe(tap((_) => console.log("error on request")));
  }

  private reset() {
    // this.data = [];
  }
}
