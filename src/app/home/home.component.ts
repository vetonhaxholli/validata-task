import { Observable } from "rxjs";
import { Component, OnInit } from "@angular/core";
import {
  FormGroup,
  FormControl,
  Validators,
  FormBuilder,
} from "@angular/forms";
import { State } from "@progress/kendo-data-query";
import { AuthService } from "../services/auth.service";
import { Edit } from "../services/edit.service";
import { User } from "../models/model";

enum PropertyType {
  "Admin" = "1",
  "Manager" = "2",
  "Editor" = "3",
  "Guide" = "4",
}

@Component({
  selector: "app-home",
  templateUrl: "./home.component.html",
  styleUrls: ["./home.component.css"],
})
export class HomeComponent implements OnInit {
  users: Observable<User[]>;
  ePropertyType = PropertyType;
  public gridState: State = {
    sort: [],
    skip: 0,
    take: 10,
  };

  public formGroup: FormGroup;
  private editedRowIndex: number;

  constructor(
    private editService: Edit,
    private _auth: AuthService,
    private formBuilder: FormBuilder
  ) {}

  ngOnInit(): void {
    this.users = this.editService.getUsers();
  }

  public onStateChange(state: State) {
    this.gridState = state;
  }

  public createFormGroup(dataItem: any): FormGroup {
    return this.formBuilder.group({
      UserId: dataItem.UserId,
      FirstName: dataItem.FirstName,
      LastName: [dataItem.LastName, Validators.required],
      Email: dataItem.Email,
      Password: dataItem.Password,
      RoleId: [dataItem.RoleId, Validators.required],
    });
  }

  public getRole(id: string): any {
    return this.ePropertyType[id];
  }

  public addHandler({ sender }) {
    this.closeEditor(sender);
    sender.addRow(this.createFormGroup(new User()));
  }

  public cellClickHandler({
    sender,
    rowIndex,
    column,
    columnIndex,
    dataItem,
    isEdited,
  }) {
    if (!isEdited && !this.isReadOnly(column.field)) {
      sender.editCell(rowIndex, columnIndex, this.createFormGroup(dataItem));
    }
  }

  private isReadOnly(field: string): boolean {
    const readOnlyColumns = ["UserId","Email"];
    return readOnlyColumns.indexOf(field) > -1;
  }

  public editHandler({ sender, rowIndex, dataItem }) {
    this.closeEditor(sender);
    this.formGroup = this.createFormGroup(dataItem);
    this.editedRowIndex = rowIndex;
    sender.editRow(rowIndex, this.formGroup);
  }

  public cancelHandler({ sender, rowIndex }) {
    this.closeEditor(sender, rowIndex);
  }

  public saveHandler({ sender, rowIndex, formGroup, isNew }) {
    const users: any[] = formGroup.value;
    this.users = this.editService.save(users, isNew);
    sender.closeRow(rowIndex);
  }

  public removeHandler({ dataItem }) {
    this.users = this.editService.remove(dataItem);
  }

  private closeEditor(grid: any, rowIndex = this.editedRowIndex) {
    grid.closeRow(rowIndex);
    this.editedRowIndex = undefined;
    this.formGroup = undefined;
  }

  public logoutUser() {
    this._auth.logoutUser();
  }
}
