import { Component, OnInit, Input } from "@angular/core";
import { AuthService } from "./services/auth.service";
import { Observable } from 'rxjs';

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
})
export class AppComponent implements OnInit {
  constructor(private _auth: AuthService) {}

  ngOnInit(): void {
  }

  isLoggedIn() {
    return (!!localStorage.getItem('token'))
  }

  logoutUser() {
    this._auth.logoutUser();
  }
}
