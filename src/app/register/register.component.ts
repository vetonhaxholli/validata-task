import { Component, OnInit } from "@angular/core";
import { AuthService } from "../services/auth.service";
import { Router } from "@angular/router";

enum PropertyType {
  'Admin' = '1',
  'Manager' = '2',
  'Editor' = '3',
  'Guide' = '4',
}

@Component({
  selector: "app-register",
  templateUrl: "./register.component.html",
  styleUrls: ["./register.component.css"],
})
export class RegisterComponent implements OnInit {
  registerUserData = {};
  ePropertyType = PropertyType;
  role:string;
  constructor(private _auth: AuthService, private _router: Router) {}

  ngOnInit() {}
  getRole(value: string) {
    this.role = value;
  }

  registerUser() {
    this.registerUserData["RoleId"] = this.role;
    this._auth.registerUser(this.registerUserData).subscribe(
      (res) => {
        localStorage.setItem("token", res.token);
        this._router.navigate(["/"]);
      },
      (err) => console.log(err)
    );
  }
}
