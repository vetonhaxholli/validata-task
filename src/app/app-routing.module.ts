import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { AppComponent } from "./app.component";
import { LoginComponent } from "./login/login.component";
import { HomeComponent } from './home/home.component';
import { AuthGuard } from './handlers/auth.guard';
import { RegisterComponent } from './register/register.component';
const routes: Routes = [
  {
    path: "login",
    component: LoginComponent, 
  },
  {
    path: "register",
    component: RegisterComponent,
  },
  {
    path: "home",
    component: HomeComponent,
    canActivate:[AuthGuard] 
  },
  {
      path: '',
      redirectTo: '/home',
      pathMatch: 'full',
      canActivate:[AuthGuard] 
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
