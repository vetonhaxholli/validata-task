import { NgModule ,NO_ERRORS_SCHEMA,CUSTOM_ELEMENTS_SCHEMA  } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import {
  HttpClientModule,
  HttpClientJsonpModule,
  HTTP_INTERCEPTORS,
} from "@angular/common/http";
import { ReactiveFormsModule, FormsModule } from "@angular/forms";
import { GridModule } from "@progress/kendo-angular-grid";
import { AppRoutingModule } from "./app-routing.module"; // CLI imports AppRoutingModule
import { AppComponent } from "./app.component";
import { LoginComponent } from "./login/login.component";
import { HomeComponent } from "./home/home.component";
import { AuthGuard } from "./handlers/auth.guard";
import { RegisterComponent } from "./register/register.component";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { TokenInterceptorService } from "./handlers/token-interceptor.service";
import { AuthService } from "./services/auth.service";

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    RegisterComponent,
  ],
  imports: [
    HttpClientModule,
    HttpClientJsonpModule,
    BrowserModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    FormsModule,
    GridModule,
    AppRoutingModule,
    NgbModule,
  ],
  providers: [
    AuthService,
    AuthGuard,
    TokenInterceptorService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptorService,
      multi: true,
    },
  ],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA]
})
export class AppModule {}
