const express = require("express");
const dataService = require('./handler')
var app = express()
var server = require('http').Server(app);
var bodyParser = require('body-parser');
const jwt = require('jsonwebtoken')
const random = require('random')

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}) );

app.all("/*", function(req, res, next){
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
    res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, Content-Length, X-Requested-With');
    next();
  });

app.get('/fetchUsers', dataService.verifyToken, function (req, res) {
    let users = dataService.fetchUsers();
    res.send(users);
})

app.post('/create', function (req, res) {
    random.int(0, 99);
    let user = req.body;
    user.UserId = random.int(0, 99) //Only for testing purpose 
    let users = dataService.addUser(user);
    res.send(users);
})

app.post('/update', function (req, res) {
    let user = req.body;
    let users = dataService.updateUser(user);
    res.send(users);
})

app.post('/remove', function (req, res) {
    let user = req.body;
    let users = dataService.removeUser(user);
    res.send(users);
})

app.post('/login', function (req, res) {
    let user = req.body;
    let users = dataService.fetchUsers();
    let result = users.find(element => element.Email == user.Email);
    if(result){
        let token = jwt.sign(result.UserId, 'validata')
        res.status(200).send({token});
    }else{
        res.status(200).send('invalid user');
    }
})

app.post('/register', function (req, res) {
    let user = req.body;
    random.int(0, 99);
    user.UserId = random.int(0, 99)
    let users = dataService.addUser(user);
    let token = jwt.sign(users, 'validata')
    res.send({token});
})


app.listen(8080, 'localhost', function(){
    console.log('Server listening')
})

