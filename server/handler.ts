const fs = require("fs");
const jwte = require('jsonwebtoken')

function readUserFile() {
  let rawdata = fs.readFileSync(__dirname + "/users.json");
  return JSON.parse(rawdata);
}

function writeUserFile(data) {
  fs.writeFileSync(__dirname + "/users.json", data);
}

function fetchUsers() {
  let users = readUserFile();
  return users;
}

function addUser(user) {
  let users = fetchUsers();
  users.push(user);
  users = JSON.stringify(users);
  writeUserFile(users);
  return users;
}

function updateUser(user) {
  let users = readUserFile();
  users.forEach((element, index) => {
    if (element.Email == user.Email) {
      users[index] = user;
    }
  });
  users = JSON.stringify(users);
  writeUserFile(users);
  return users;
}

function removeUser(user) {
  let users = readUserFile();
  users.forEach((element, index) => {
    if (element.UserId == user.UserId) {
      users.splice(index, 1);
    }
  });
  users = JSON.stringify(users);
  writeUserFile(users);
  return users;
}

function verifyToken(req, res, next) {
  if (!req.headers.authorization) {
    return res.status(200).send("Unauthorized Request");
  }
  let token = req.headers.authorization.split(" ")[1];
  if (token) {
    let payload = jwte.verify(token, "validata");
    if (payload) {
      next();
    } else {
      return res.status(200).send("Unauthorized Request");
    }
  }
}

module.exports = {
  fetchUsers: fetchUsers,
  updateUser: updateUser,
  addUser: addUser,
  removeUser: removeUser,
  verifyToken:verifyToken
};
